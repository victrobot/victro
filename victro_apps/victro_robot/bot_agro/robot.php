<?php
class BotAgro extends controller_robot {
    public function language($bot_pag = 1, $bot_mod = 1){
        $bot_data = $this->global_robot();
        $bot_model = $this->model("language");
        $bot_data['returned'] = $bot_model->select_language($bot_pag)->get_fetch();
        $bot_data['pags'] = $bot_model->bot_numpags;
        if($bot_mod == 1){
            $this->system_view("language", $bot_data);
        } else {
            $this->view("language", $bot_data);
        }
    }
    public function cadPlanta(){
        $bot_data = $this->global_robot();
        $bot_model = $this->model("language");
				$this->system_view('cadPlanta');				
    }
    
    public function testAddon(){
        $bot_agro = $this->addon(1);
        $bot_agro->hard("GPIO")->pin(1)->type("O")->Volt("H")->set();
        $bot_agro->hard("GPIO")->pin(2)->type("I")->Volt("H")->set();
        //$bot_agro->hard("GPIO")->pin(1)->type("O")->Volt("L")->set();
        $bot_agro->send();
    }
}

?>