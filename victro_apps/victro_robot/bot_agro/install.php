<?php

$victro_robot->name("BotAgro");
$victro_robot->try_route("agro");
$victro_robot->try_route("Agro");
$victro_robot->author("Helen Dias");
$victro_robot->description("Agro");
$victro_robot->version(1.0);
$victro_robot->icon("fa fa-heart-o");
$victro_robot->update("http://victrobot.com/update/[KEY]");

//TABLE INFORMATION
$victro_robot->table("agro")->engine("INNODB")->if_table("NOT EXISTS");
$victro_robot->column("ID")->type("INT")->value("11")->autoincrement(true)->index("PRIMARY KEY");
$victro_robot->column("name")->type("varchar")->value("100");
$victro_robot->column("class")->type('INT')->value('10');
$victro_robot->column("order")->type('INT')->value('10');
$victro_robot->column("family")->type('INT')->value('10');
$victro_robot->column("genus")->type('varchar')->value('150');
$victro_robot->column("species")->type('varchar')->value('200');
$victro_robot->column("soil_humidity")->type('INT')->value('5');
$victro_robot->column("air_humidity")->type('double')->value('');
$victro_robot->column("air_temperature")->type('double')->value('');
var_dump($_SESSION['debug_create_tables']);

//MENU INFORMATION
$victro_robot->menu("BOT AGRO", "3")->submenu('agro', '3');
?>