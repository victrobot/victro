<?php
	if(isset($_POST['check'])){
		$victro_charset = "UTF-8";
		$victro_smtp = $_POST['smtp'];
		$victro_email = $_POST['email'];
		$victro_pass = $_POST['pass1'];
		$victro_port = $_POST['port'];
		$victro_name = $_POST['name'];
		include_once('../victro_system/classes/phpmailer/PHPMailerAutoload.php');
		$victro_mail = new PHPMailer();
		$victro_mail->CharSet = $victro_charset;
		$victro_mail->isSMTP();                                      // Set mailer to use SMTP
		$victro_mail->Host = $victro_smtp;  // Specify main and backup SMTP servers
		$victro_mail->SMTPAuth = true;                               // Enable SMTP authentication
		$victro_mail->Username = $victro_email;                 // SMTP username
		$victro_mail->Password = $victro_pass;                           // SMTP password
		$victro_mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$victro_mail->Port = $victro_port;                                    // TCP port to connect to
		$victro_mail->From = $victro_email; 
		$victro_mail->FromName = $victro_name; 
		$victro_mail->addAddress($victro_email, $victro_name);     // Add a recipient
		$victro_mail->addReplyTo($victro_email, $victro_name);
		
		$victro_mail->isHTML(true);                                   // Set email format to HTML
		
		$victro_mail->Subject = "Email test";
		
		$victro_mail->Body    = "Testing SMTP";
		$victro_mail->AltBody = "Testing SMTP";

		if($victro_mail->send()) {
			$victro_tb2 = $victro_connect->prepare("insert into victro_sendmail values(null, :name, :email, :pass, :smtp, :port, :charset)");
			$victro_tb2->bindParam(":name", $victro_name, PDO::PARAM_STR); 
			$victro_tb2->bindParam(":email", $victro_email, PDO::PARAM_STR);
			$victro_tb2->bindParam(":pass", $victro_pass, PDO::PARAM_STR);
			$victro_tb2->bindParam(":smtp", $victro_smtp, PDO::PARAM_STR);
			$victro_tb2->bindParam(":port", $victro_port, PDO::PARAM_STR);
			$victro_tb2->bindParam(":charset", $victro_charset, PDO::PARAM_STR);
			$victro_tb2->execute();
			header("Refresh:0");
		} else {
			echo '<center><h1>INVALID SMTP CONNECTION</h1></center>';
		}
	}