<?php

function bot_translate($victro_string, $victro_type = 1, $victro_return = false, $victro_force = false) {
    if($victro_force === false){
        GLOBAL $victro_language;
    } else {
        GLOBAL $victro_robot;
        $victro_language_file = PATH_APP.PATH_ROBOT.$victro_robot['local_url']."/language/".$victro_force.".php";
        if(file_exists($victro_language_file)){
            include_once($victro_language_file);
        }
    }
    $victro_string3 = str_replace(" ", "_", $victro_string);
    $victro_string3 = mb_strtolower($victro_string3, "UTF-8");
    if (isset($victro_language[$victro_string3])) {
        $victro_newstring = $victro_language[$victro_string3];
    } else {
        $victro_string2 = explode(' ', $victro_string);
        $victro_i = 0;
        $victro_newstring1 = array();
        foreach ($victro_string2 as $victro_searchword) {
            $victro_searchword = mb_strtolower($victro_searchword, "UTF-8");
            if (isset($victro_string2[$victro_i - 1]) and isset($victro_string2[$victro_i]) and isset($victro_string2[$victro_i + 1])) {
                $victro_searchword1 = mb_strtolower($victro_string2[$victro_i - 1] . '_' . $victro_string2[$victro_i] . '_' . $victro_string2[$victro_i + 1], "UTF-8");
                if (isset($victro_language[$victro_searchword1])) {
                    $victro_newstring1[] = $victro_language[$victro_searchword1];
                    unset($victro_string2[$victro_i]);
                    unset($victro_string2[$victro_i - 1]);
                    unset($victro_string2[$victro_i + 1]);
                } else if (isset($victro_string2[$victro_i]) and isset($victro_string2[$victro_i + 1])) {
                    $victro_searchword3 = mb_strtolower($victro_string2[$victro_i] . '_' . $victro_string2[$victro_i + 1], "UTF-8");
                    if (isset($victro_language[$victro_searchword3])) {
                        $victro_newstring1[] = $victro_language[$victro_searchword3];
                        unset($victro_string2[$victro_i]);
                        unset($victro_string2[$victro_i + 1]);
                    } else if (isset($victro_string2[$victro_i]) and isset($victro_language[$victro_searchword])) {
                        $victro_newstring1[] = $victro_language[$victro_searchword];
                        unset($victro_string2[$victro_i]);
                    } else if (isset($victro_string2[$victro_i])) {
                        $victro_newstring1[] = $victro_searchword;
                        unset($victro_string2[$victro_i]);
                    }
                } else if (isset($victro_string2[$victro_i]) and isset($victro_language[$victro_searchword])) {
                    $victro_newstring1[] = $victro_language[$victro_searchword];
                    unset($victro_string2[$victro_i]);
                } else if (isset($victro_string2[$victro_i])) {
                    $victro_newstring1[] = $victro_searchword;
                    unset($victro_string2[$victro_i]);
                }
            } else if (isset($victro_string2[$victro_i]) and isset($victro_string2[$victro_i + 1])) {
                $victro_searchword3 = mb_strtolower($victro_string2[$victro_i] . '_' . $victro_string2[$victro_i + 1], "UTF-8");
                if (isset($victro_language[$victro_searchword3])) {
                    $victro_newstring1[] = $victro_language[$victro_searchword3];
                    unset($victro_string2[$victro_i]);
                    unset($victro_string2[$victro_i + 1]);
                } else if (isset($victro_string2[$victro_i]) and isset($victro_language[$victro_searchword])) {
                    $victro_newstring1[] = $victro_language[$victro_searchword];
                    unset($victro_string2[$victro_i]);
                } else if (isset($victro_string2[$victro_i])) {
                    $victro_newstring1[] = $victro_searchword;
                    unset($victro_string2[$victro_i]);
                }
            } else if (isset($victro_string2[$victro_i]) and isset($victro_language[$victro_searchword])) {
                $victro_newstring1[] = $victro_language[$victro_searchword];
                unset($victro_string2[$victro_i]);
            } else if (isset($victro_string2[$victro_i])) {
                $victro_newstring1[] = $victro_searchword;
                unset($victro_string2[$victro_i]);
            }
            $victro_i++;
        }
        $victro_newstring = implode(' ', $victro_newstring1);
    }
    $victro_newstring = mb_strtolower($victro_newstring, "UTF-8");
    if ($victro_type == 2) {
        $victro_newstring = mb_strtoupper($victro_newstring, "UTF-8");
    } else if ($victro_type == 3) {
        $victro_newstring = mb_strtolower($victro_newstring, "UTF-8");
    } else {
        $victro_newstring = ucfirst($victro_newstring);
    }
    if ($victro_return == false) {
        echo $victro_newstring;
    } else {
        return($victro_newstring);
    }
}
function bot_translate2($victro_string, $victro_type = 1, $victro_return = false, $victro_force = false) {
    if($victro_force === false){
        GLOBAL $victro_language;
    } else {
        GLOBAL $victro_robot;
        $victro_language_file = PATH_APP.PATH_ROBOT.$victro_robot['local_url']."/language/".$victro_force.".php";
        if(file_exists($victro_language_file)){
            include($victro_language_file);
        }
    }
    $victro_newstring = $victro_string;
    $victro_array = explode(" ", $victro_string);
    $victro_pos = count($victro_array);
    $victro_count = 0;
    for($i = 1; $i <= $victro_pos; $i++){
        $victro_step = $victro_pos - $i + 1;
        for($j = 0; $j < $i; $j++){
            $victro_tra = "";
            for($k = 0; $k < $victro_step; $k++){
                if($victro_tra == ""){
                    $victro_tra = $victro_array[$k+$j];
                } else {
                    $victro_tra = $victro_tra." ".$victro_array[$k+$j];
                }
            }
            $victro_compare = mb_strtolower(str_replace(" ", "_", $victro_tra));
            if(isset($victro_language[$victro_compare])){
                $victro_count = $victro_count + $victro_step;
                $victro_newstring = str_replace($victro_tra, $victro_language[$victro_compare], $victro_newstring);
            }
            if($victro_count >= $victro_pos + 1){
                break;
            }
        }
    }
    $victro_newstring = mb_strtolower($victro_newstring, "UTF-8");
    if ($victro_type == 2) {
        $victro_newstring = mb_strtoupper($victro_newstring, "UTF-8");
    } else if ($victro_type == 3) {
        $victro_newstring = mb_strtolower($victro_newstring, "UTF-8");
    } else {
        $victro_newstring = ucfirst($victro_newstring);
    }
    if ($victro_return == false) {
        echo $victro_newstring;
    } else {
        return($victro_newstring);
    }
}

?>