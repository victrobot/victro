<?php
	/******
	 *
	 *	EditArea PHP compressor
	 * 	Developped by Christophe Dolivet
	 *	Released under LGPL, Apache and BSD licenses
	 *	v1.1.3 (2007/01/18)	 
	 *
	******/
	
	// CONFIG
	$victro_param['cache_duration']= 3600 * 24 * 10;		// 10 days util client cache expires
	$victro_param['compress'] = true;						// enable the code compression, should be activated but it can be usefull to desactivate it for easier error retrieving (true or false)
	$victro_param['debug'] = false;						// Enable this option if you need debuging info
	$victro_param['use_disk_cache']= true;					// If you enable this option gzip files will be cached on disk.
	$victro_param['use_gzip']= true;						// Enable gzip compression
	// END CONFIG
	
	$victro_compressor= new Compressor($victro_param);
	
	class Compressor{
	
		
		function compressor($victro_param)
		{
			$this->__construct($victro_param);
		}
		
		function __construct($victro_param)
		{
			$this->start_time= $this->get_microtime();
			$this->file_loaded_size=0;
			$this->param= $victro_param;
			$this->script_list="";
			$this->path= dirname(__FILE__)."/";
			if(isset($_GET['plugins'])){
				$this->load_all_plugins= true;
				$this->full_cache_file= $this->path."edit_area_full_with_plugins.js";
				$this->gzip_cache_file= $this->path."edit_area_full_with_plugins.gz";
			}else{
				$this->load_all_plugins= false;
				$this->full_cache_file= $this->path."edit_area_full.js";
				$this->gzip_cache_file= $this->path."edit_area_full.gz";
			}
			
			$this->check_gzip_use();
			$this->send_headers();
			$this->check_cache();
			$this->load_files();
			$this->send_datas();
		}
		
		function send_headers()
		{
			header("Content-type: text/javascript; charset: UTF-8");
			header("Vary: Accept-Encoding"); // Handle proxies
			header(sprintf("Expires: %s GMT", gmdate("D, d M Y H:i:s", time() + $this->param['cache_duration'])) );
			if($this->use_gzip)
				header("Content-Encoding: ".$this->gzip_enc_header);
		}
		
		function check_gzip_use()
		{
			$victro_encodings = array();
			$victro_desactivate_gzip=false;
					
			if (isset($_SERVER['HTTP_ACCEPT_ENCODING']))
				$victro_encodings = explode(',', strtolower(preg_replace("/\s+/", "", $_SERVER['HTTP_ACCEPT_ENCODING'])));
			
			// desactivate gzip for IE version < 7
			if(preg_match("/(?:msie )([0-9.]+)/i", $_SERVER['HTTP_USER_AGENT'], $victro_ie))
			{
				if($victro_ie[1]<7)
					$victro_desactivate_gzip=true;	
			}
			
			// Check for gzip header or northon internet securities
			if (!$victro_desactivate_gzip && $this->param['use_gzip'] && (in_array('gzip', $victro_encodings) || in_array('x-gzip', $victro_encodings) || isset($_SERVER['---------------'])) && function_exists('ob_gzhandler') && !ini_get('zlib.output_compression')) {
				$this->gzip_enc_header= in_array('x-gzip', $victro_encodings) ? "x-gzip" : "gzip";
				$this->use_gzip=true;
				$this->cache_file=$this->gzip_cache_file;
			}else{
				$this->use_gzip=false;
				$this->cache_file=$this->full_cache_file;
			}
		}
		
		function check_cache()
		{
			// Only gzip the contents if clients and server support it
			if (file_exists($this->cache_file)) {
				// check if cache file must be updated
				$victro_cache_date=0;				
				if ($victro_dir = opendir($this->path)) {
					while (($victro_file = readdir($victro_dir)) !== false) {
						if(is_file($this->path.$victro_file) && $victro_file!="." && $victro_file!="..")
							$victro_cache_date= max($victro_cache_date, filemtime($this->path.$victro_file));
					}
					closedir($victro_dir);
				}
				if($this->load_all_plugins){
					$victro_plug_path= $this->path."plugins/";
					if (($victro_dir = @opendir($victro_plug_path)) !== false)
					{
						while (($victro_file = readdir($victro_dir)) !== false)
						{
							if ($victro_file !== "." && $victro_file !== "..")
							{
								if(is_dir($victro_plug_path.$victro_file) && file_exists($victro_plug_path.$victro_file."/".$victro_file.".js"))
									$victro_cache_date= max($victro_cache_date, filemtime("plugins/".$victro_file."/".$victro_file.".js"));
							}
						}
						closedir($victro_dir);
					}
				}

				if(filemtime($this->cache_file) >= $victro_cache_date){
					// if cache file is up to date
					$victro_last_modified = gmdate("D, d M Y H:i:s",filemtime($this->cache_file))." GMT";
					if (isset($_SERVER["HTTP_IF_MODIFIED_SINCE"]) && strcasecmp($_SERVER["HTTP_IF_MODIFIED_SINCE"], $victro_last_modified) === 0)
					{
						header("HTTP/1.1 304 Not Modified");
						header("Last-modified: ".$victro_last_modified);
						header("Cache-Control: Public"); // Tells HTTP 1.1 clients to cache
						header("Pragma:"); // Tells HTTP 1.0 clients to cache
					}
					else
					{
						header("Last-modified: ".$victro_last_modified);
						header("Cache-Control: Public"); // Tells HTTP 1.1 clients to cache
						header("Pragma:"); // Tells HTTP 1.0 clients to cache
						header('Content-Length: '.filesize($this->cache_file));
						echo file_get_contents($this->cache_file);
					}				
					die;
				}
			}
			return false;
		}
		
		function load_files()
		{
			$victro_loader= $this->get_content("edit_area_loader.js")."\n";
			
			// get the list of other files to load
	    	$victro_loader= preg_replace("/(t\.scripts_to_load=\s*)\[([^\]]*)\];/e"
						, "\$this->replace_scripts('script_list', '\\1', '\\2')"
						, $victro_loader);
		
			$victro_loader= preg_replace("/(t\.sub_scripts_to_load=\s*)\[([^\]]*)\];/e"
						, "\$this->replace_scripts('sub_script_list', '\\1', '\\2')"
						, $victro_loader);

			// replace languages names
			$victro_reg_path= $this->path."reg_syntax/";
			$victro_a_displayName	= array();
			if (($victro_dir = @opendir($victro_reg_path)) !== false)
			{
				while (($victro_file = readdir($victro_dir)) !== false)
				{
					if( $victro_file !== "." && $victro_file !== ".." && ( $victro_pos = strpos( $victro_file, '.js' ) ) !== false )
					{
						$victro_jsContent	= $this->file_get_contents( $victro_reg_path.$victro_file );
						if( preg_match( '@(\'|")DISPLAY_NAME\1\s*:\s*(\'|")(.*)\2@', $victro_jsContent, $victro_match ) )
						{
							$victro_a_displayName[] = "'". substr( $victro_file, 0, $victro_pos ) ."':'". htmlspecialchars( $victro_match[3], ENT_QUOTES ) ."'";
						}
					}
				}
				closedir($victro_dir);
			}
			$victro_loader	= str_replace( '/*syntax_display_name_AUTO-FILL-BY-COMPRESSOR*/', implode( ",", $victro_a_displayName ), $victro_loader );
						
			$this->datas= $victro_loader;
			$this->compress_javascript($this->datas);
			
			// load other scripts needed for the loader
			preg_match_all('/"([^"]*)"/', $this->script_list, $victro_match);
			foreach($victro_match[1] as $victro_key => $victro_value)
			{
				$victro_content= $this->get_content(preg_replace("/\\|\//i", "", $victro_value).".js");
				$this->compress_javascript($victro_content);
				$this->datas.= $victro_content."\n";
			}
			//$this->datas);
			//$this->datas= preg_replace('/(( |\t|\r)*\n( |\t)*)+/s', "", $this->datas);
			
			// improved compression step 1/2	
			$this->datas= preg_replace(array("/(\b)EditAreaLoader(\b)/", "/(\b)editAreaLoader(\b)/", "/(\b)editAreas(\b)/"), array("EAL", "eAL", "eAs"), $this->datas);
			//$this->datas= str_replace(array("EditAreaLoader", "editAreaLoader", "editAreas"), array("EAL", "eAL", "eAs"), $this->datas);
			$this->datas.= "var editAreaLoader= eAL;var editAreas=eAs;EditAreaLoader=EAL;";
		
			// load sub scripts
			$victro_sub_scripts="";
			$victro_sub_scripts_list= array();
			preg_match_all('/"([^"]*)"/', $this->sub_script_list, $victro_match);
			foreach($victro_match[1] as $victro_value){
				$victro_sub_scripts_list[]= preg_replace("/\\|\//i", "", $victro_value).".js";
			}
		
			if($this->load_all_plugins){
				// load plugins scripts
				$victro_plug_path= $this->path."plugins/";
				if (($victro_dir = @opendir($victro_plug_path)) !== false)
				{
					while (($victro_file = readdir($victro_dir)) !== false)
					{
						if ($victro_file !== "." && $victro_file !== "..")
						{
							if(is_dir($victro_plug_path.$victro_file) && file_exists($victro_plug_path.$victro_file."/".$victro_file.".js"))
								$victro_sub_scripts_list[]= "plugins/".$victro_file."/".$victro_file.".js";
						}
					}
					closedir($victro_dir);
				}
			}
							
			foreach($victro_sub_scripts_list as $victro_value){
				$victro_sub_scripts.= $this->get_javascript_content($victro_value);
			}
			// improved compression step 2/2	
			$victro_sub_scripts= preg_replace(array("/(\b)editAreaLoader(\b)/", "/(\b)editAreas(\b)/", "/(\b)editArea(\b)/", "/(\b)EditArea(\b)/"), array("eAL", "eAs", "eA", "EA"), $victro_sub_scripts);
		//	$victro_sub_scripts= str_replace(array("editAreaLoader", "editAreas", "editArea", "EditArea"), array("eAL", "eAs", "eA", "EA"), $victro_sub_scripts);
			$victro_sub_scripts.= "var editArea= eA;EditArea=EA;";
			
			
			// add the scripts
		//	$this->datas.= sprintf("editAreaLoader.iframe_script= \"<script type='text/javascript'>%s</script>\";\n", $victro_sub_scripts);
		
		
			// add the script and use a last compression 
			if( $this->param['compress'] )
			{
				$victro_last_comp	= array( 'Á' => 'this',
								 'Â' => 'textarea',
								 'Ã' => 'function',
								 'Ä' => 'prototype',
								 'Å' => 'settings',
								 'Æ' => 'length',
								 'Ç' => 'style',
								 'È' => 'parent',
								 'É' => 'last_selection',
								 'Ê' => 'value',
								 'Ë' => 'true',
								 'Ì' => 'false'
								 /*,
									'Î' => '"',
								 'Ï' => "\n",
								 'À' => "\r"*/);
			}
			else
			{
				$victro_last_comp	= array();
			}
			
			$victro_js_replace= '';
			foreach( $victro_last_comp as $victro_key => $victro_val )
				$victro_js_replace .= ".replace(/". $victro_key ."/g,'". str_replace( array("\n", "\r"), array('\n','\r'), $victro_val ) ."')";
			
			$this->datas.= sprintf("editAreaLoader.iframe_script= \"<script type='text/javascript'>%s</script>\"%s;\n",
								str_replace( array_values($victro_last_comp), array_keys($victro_last_comp), $victro_sub_scripts ), 
								$victro_js_replace);
			
			if($this->load_all_plugins)
				$this->datas.="editAreaLoader.all_plugins_loaded=true;\n";
		
			
			// load the template
			$this->datas.= sprintf("editAreaLoader.template= \"%s\";\n", $this->get_html_content("template.html"));
			// load the css
			$this->datas.= sprintf("editAreaLoader.iframe_css= \"<style>%s</style>\";\n", $this->get_css_content("edit_area.css"));
					
		//	$this->datas= "function editArea(){};editArea.prototype.loader= function(){alert('bouhbouh');} var a= new editArea();a.loader();";
					
		}
		
		function send_datas()
		{
			if($this->param['debug']){
				$victro_header=sprintf("/* USE PHP COMPRESSION\n");
				$victro_header.=sprintf("javascript size: based files: %s => PHP COMPRESSION => %s ", $this->file_loaded_size, strlen($this->datas));
				if($this->use_gzip){
					$victro_gzip_datas=  gzencode($this->datas, 9, FORCE_GZIP);				
					$victro_header.=sprintf("=> GZIP COMPRESSION => %s", strlen($victro_gzip_datas));
					$victro_ratio = round(100 - strlen($victro_gzip_datas) / $this->file_loaded_size * 100.0);			
				}else{
					$victro_ratio = round(100 - strlen($this->datas) / $this->file_loaded_size * 100.0);
				}
				$victro_header.=sprintf(", reduced by %s%%\n", $victro_ratio);
				$victro_header.=sprintf("compression time: %s\n", $this->get_microtime()-$this->start_time); 
				$victro_header.=sprintf("%s\n", implode("\n", $this->infos));
				$victro_header.=sprintf("*/\n");
				$this->datas= $victro_header.$this->datas;	
			}
			$victro_mtime= time(); // ensure that the 2 disk files will have the same update time
			// generate gzip file and cahce it if using disk cache
			if($this->use_gzip){
				$this->gzip_datas= gzencode($this->datas, 9, FORCE_GZIP);
				if($this->param['use_disk_cache'])
					$this->file_put_contents($this->gzip_cache_file, $this->gzip_datas, $victro_mtime);
			}
			
			// generate full js file and cache it if using disk cache			
			if($this->param['use_disk_cache'])
				$this->file_put_contents($this->full_cache_file, $this->datas, $victro_mtime);
			
			// generate output
			if($this->use_gzip)
				echo $this->gzip_datas;
			else
				echo $this->datas;
				
//			die;
		}
				
		
		function get_content($victro_end_uri)
		{
			$victro_end_uri=preg_replace("/\.\./", "", $victro_end_uri); // Remove any .. (security)
			$victro_file= $this->path.$victro_end_uri;
			if(file_exists($victro_file)){
				$this->infos[]=sprintf("'%s' loaded", $victro_end_uri);
				/*$victro_fd = fopen($victro_file, 'rb');
				$victro_content = fread($victro_fd, filesize($victro_file));
				fclose($victro_fd);
				return $victro_content;*/
				return $this->file_get_contents($victro_file);
			}else{
				$this->infos[]=sprintf("'%s' not loaded", $victro_end_uri);
				return "";
			}
		}
		
		function get_javascript_content($victro_end_uri)
		{
			$victro_val=$this->get_content($victro_end_uri);
	
			$this->compress_javascript($victro_val);
			$this->prepare_string_for_quotes($victro_val);
			return $victro_val;
		}
		
		function compress_javascript(&$victro_code)
		{
			if($this->param['compress'])
			{
				// remove all comments
				//	(\"(?:[^\"\\]*(?:\\\\)*(?:\\\"?)?)*(?:\"|$victro_))|(\'(?:[^\'\\]*(?:\\\\)*(?:\\'?)?)*(?:\'|$victro_))|(?:\/\/(?:.|\r|\t)*?(\n|$victro_))|(?:\/\*(?:.|\n|\r|\t)*?(?:\*\/|$victro_))
				$victro_code= preg_replace("/(\"(?:[^\"\\\\]*(?:\\\\\\\\)*(?:\\\\\"?)?)*(?:\"|$victro_))|(\'(?:[^\'\\\\]*(?:\\\\\\\\)*(?:\\\\\'?)?)*(?:\'|$victro_))|(?:\/\/(?:.|\r|\t)*?(\n|$victro_))|(?:\/\*(?:.|\n|\r|\t)*?(?:\*\/|$victro_))/s", "$victro_1$victro_2$victro_3", $victro_code);
				// remove line return, empty line and tabulation
				$victro_code= preg_replace('/(( |\t|\r)*\n( |\t)*)+/s', " ", $victro_code);
				// add line break before "else" otherwise navigators can't manage to parse the file
				$victro_code= preg_replace('/(\b(else)\b)/', "\n$victro_1", $victro_code);
				// remove unnecessary spaces
				$victro_code= preg_replace('/( |\t|\r)*(;|\{|\}|=|==|\-|\+|,|\(|\)|\|\||&\&|\:)( |\t|\r)*/', "$victro_2", $victro_code);
			}
		}
		
		function get_css_content($victro_end_uri){
			$victro_code=$this->get_content($victro_end_uri);
			// remove comments
			$victro_code= preg_replace("/(?:\/\*(?:.|\n|\r|\t)*?(?:\*\/|$victro_))/s", "", $victro_code);
			// remove spaces
			$victro_code= preg_replace('/(( |\t|\r)*\n( |\t)*)+/s', "", $victro_code);
			// remove spaces
			$victro_code= preg_replace('/( |\t|\r)?(\:|,|\{|\})( |\t|\r)+/', "$victro_2", $victro_code);
		
			$this->prepare_string_for_quotes($victro_code);
			return $victro_code;
		}
		
		function get_html_content($victro_end_uri){
			$victro_code=$this->get_content($victro_end_uri);
			//$victro_code= preg_replace('/(\"(?:\\\"|[^\"])*(?:\"|$victro_))|' . "(\'(?:\\\'|[^\'])*(?:\'|$victro_))|(?:\/\/(?:.|\r|\t)*?(\n|$victro_))|(?:\/\*(?:.|\n|\r|\t)*?(?:\*\/|$victro_))/s", "$victro_1$victro_2$victro_3", $victro_code);
			$victro_code= preg_replace('/(( |\t|\r)*\n( |\t)*)+/s', " ", $victro_code);
			$this->prepare_string_for_quotes($victro_code);
			return $victro_code;
		}
		
		function prepare_string_for_quotes(&$victro_str){
			// prepare the code to be putted into quotes 
			/*$victro_pattern= array("/(\\\\)?\"/", '/\\\n/'	, '/\\\r/'	, "/(\r?\n)/");
			$victro_replace= array('$victro_1$victro_1\\"', '\\\\\\n', '\\\\\\r'	, '\\\n"$victro_1+"');*/
			$victro_pattern= array("/(\\\\)?\"/", '/\\\n/'	, '/\\\r/'	, "/(\r?\n)/");
			if($this->param['compress'])
				$victro_replace= array('$victro_1$victro_1\\"', '\\\\\\n', '\\\\\\r'	, '\n');
			else
				$victro_replace= array('$victro_1$victro_1\\"', '\\\\\\n', '\\\\\\r'	, "\\n\"\n+\"");
			$victro_str= preg_replace($victro_pattern, $victro_replace, $victro_str);
		}
		
		function replace_scripts($victro_var, $victro_param1, $victro_param2)
		{
			$this->$victro_var=stripslashes($victro_param2);
	        return $victro_param1."[];";
		}

		/* for php version that have not thoses functions */
		function file_get_contents($victro_file)
		{
			$victro_fd = fopen($victro_file, 'rb');
			$victro_content = fread($victro_fd, filesize($victro_file));
			fclose($victro_fd);
			$this->file_loaded_size+= strlen($victro_content);
			return $victro_content;				
		}
		
		function file_put_contents($victro_file, &$victro_content, $victro_mtime=-1)
		{
			if($victro_mtime==-1)
				$victro_mtime=time();
			$victro_fp = @fopen($victro_file, "wb");
			if ($victro_fp) {
				fwrite($victro_fp, $victro_content);
				fclose($victro_fp);
				touch($victro_file, $victro_mtime);
				return true;
			}
			return false;
		}
		
		function get_microtime()
		{
		   list($victro_usec, $victro_sec) = explode(" ", microtime());
		   return ((float)$victro_usec + (float)$victro_sec);
		}
	}	
?>
